package com.gyungdal.tcpfilesend.network;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/*
 * Created by GyungDal on 2016-04-15.
 */
public class Send extends AsyncTask<Object, Object, Boolean> {
    private static final boolean SUCESS = true;
    private static final boolean FAIL = false;
    private static final String TAG = "Network Send";
    private String serverIp;
    private int port;
    private static final String IMAGE_NAME = "TEST.png";
    private static final String VIDEO_NAME = "TEST.mp4";
    public Send(String serverIp, int port){
        this.serverIp = serverIp;
        this.port = port;
    }

    @Override
    protected Boolean doInBackground(Object... params) {
        try{
            Log.d(TAG, "Server Connect");
            InetAddress serverAddr = InetAddress.getByName(serverIp);
            Socket sock = new Socket(serverAddr, port);
            DataInputStream input = new DataInputStream(sock.getInputStream());
            DataOutputStream output = new DataOutputStream(sock.getOutputStream());
            WriteSocket(output,
                    Environment.getExternalStorageDirectory().getAbsolutePath() +
                    File.separator + Environment.DIRECTORY_DOWNLOADS,
                    VIDEO_NAME);

        } catch (UnknownHostException e) {
            Log.e(TAG, e.getMessage());
            return FAIL;
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            return FAIL;
        }
        return SUCESS;
    }
    public void WriteSocket(DataOutputStream output, String Path, String name)
        throws IOException{
        output.writeUTF(name);
        Log.i(TAG, name + " 파일 전송");

        File origin = new File(Path + File.separator + name);
        FileInputStream fis = new FileInputStream(origin);
        BufferedInputStream bis = new BufferedInputStream(fis);
        int len;
        long size = origin.length();
        output.writeUTF(String.valueOf(size));
        byte[] data = new byte[(int)size];
        while ((len = bis.read(data)) != -1) {
            output.write(data, 0, len);
        }
        output.flush();
        output.close();
        bis.close();
        fis.close();
    }

    public void ReadSocket(DataInputStream data)
        throws IOException{
        byte[] datafile = null;
        data.read(datafile);
    }
}
